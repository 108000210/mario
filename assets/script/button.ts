
const {ccclass, property} = cc._decorator;

@ccclass
export default class button extends cc.Component {

    

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start(){
        var hit = new cc.Component.EventHandler();
        hit.target = this.node;
        hit.component = "button";
        if(this.node.name == 'login'){
            hit.handler = "on_login";
        }else if(this.node.name == 'play'){
            hit.handler = "on_play";
        }
        //hit.handler = "on_click";
        this.node.getComponent(cc.Button).clickEvents.push(hit);
    }

    on_login(){
        cc.director.loadScene("login");
    }

    on_play(){
        cc.director.loadScene("level_select");
    }
    // update (dt) {}
}
