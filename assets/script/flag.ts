
const {ccclass, property} = cc._decorator;

@ccclass
export default class flag extends cc.Component {

    @property({ type: cc.AudioClip })
    private clear_bgm: cc.AudioClip = null;


    onBeginContact(contact, self, other){
        //cc.audioEngine.playMusic(this.clear_bgm, false);
        if(other.node.name == 'mario'){
            cc.find("gameM").getComponent('gameM').store_usr_info();
            cc.audioEngine.playMusic(this.clear_bgm, false);
            cc.find("gameM").getComponent('gameM').load_win_scene();
            
        }
    }
}
