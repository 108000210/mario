
const {ccclass, property} = cc._decorator;

@ccclass
export default class level_select extends cc.Component {

    

    start () {
        var hit = new cc.Component.EventHandler();
        hit.target = this.node;
        hit.component = "level_select";
        if(this.node.name == 'level1'){
            hit.handler = "on_level1";
        }else if(this.node.name == 'level2'){
            hit.handler = "on_level2";
        }
        //hit.handler = "on_click";
        this.node.getComponent(cc.Button).clickEvents.push(hit);
    }

    on_level1(){
        cc.director.loadScene("world1_start");
    }

    on_level2(){
        cc.director.loadScene("world2_start");
    }

    // update (dt) {}
}
