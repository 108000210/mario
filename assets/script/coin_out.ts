
const {ccclass, property} = cc._decorator;

@ccclass
export default class coin_out extends cc.Component {

    
    @property({type: cc.AudioClip})
    private coin: cc.AudioClip = null;
    // LIFE-CYCLE CALLBACKS:
    
    public show(){
        let action = cc.moveBy(0.3, 0, 100);
        let action2 = cc.fadeOut(0.2);
        let action3 = cc.callFunc(()=>{this.node.destroy();});
        this.node.runAction(cc.sequence(action, action2, action3));
        cc.audioEngine.playEffect(this.coin,false);
    }
}
