
const {ccclass, property} = cc._decorator;

@ccclass
export default class usr_login extends cc.Component {

    

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    start(){
        var hit = new cc.Component.EventHandler();
        hit.target = this.node;
        hit.component = "usr_login";
        if(this.node.name == 'button_blue'){
            hit.handler = "on_click";
        }else if(this.node.name == 'button_orange'){
            hit.handler = "on_back";
        }
        this.node.getComponent(cc.Button).clickEvents.push(hit);
    }

    on_click(){
        let email = cc.find("email").getComponent(cc.EditBox).string;
        let password = cc.find("password").getComponent(cc.EditBox).string;
        firebase.auth().signInWithEmailAndPassword(email, password).then((result)=>{
            cc.log('log in success!');
            alert('log in success!');
            cc.director.loadScene("level_select");
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            cc.log(errorMessage);
            if(errorCode == 'auth/user-not-found'){
                firebase.auth().createUserWithEmailAndPassword(email,password).then(()=>{
                    firebase.auth().signInWithEmailAndPassword(email, password).then(()=>{
                        cc.log('sign up and log in success!');
                        cc.director.loadScene("level_select");
                    });
                }).catch((error)=>{
                    cc.log(error.message);
                });
            }
            
        });


    }
    on_back(){
        cc.director.loadScene("menu");
    }
    // update (dt) {}
}
