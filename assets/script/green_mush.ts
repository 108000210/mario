
const {ccclass, property} = cc._decorator;

@ccclass
export default class green_mush extends cc.Component {

    

    // LIFE-CYCLE CALLBACKS:
    private Start: boolean = false;

    private speed: number = 200;
    // onLoad () {}
    start(){
        this.show();
    }
    

    update (dt) {
        //let sy = this.node.getComponent(cc.RigidBody).linearVelocity.y;
        if(this.Start){
            let sy = this.node.getComponent(cc.RigidBody).linearVelocity.y;
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.speed, sy);
            //this.node.x += this.speed*dt;
        }
        if(this.node.getBoundingBoxToWorld().y < 0) this.node.destroy();
    }
    show(){
        this.Start = true;
    }
    onBeginContact(contact, self, other){
        if(this.Start){
            if(other.node.name == 'mario'){
                other.node.getComponent('player').turn_green_anim();
                if(other.node.getComponent('player').player_status == 1){
                    other.node.getComponent('player').player_status += 1;
                }
                //other.node.getComponent('player').player_status += 1;
                //cc.log(other.node.getComponent('player').player_status);
                this.node.destroy();
            }
            if(other.node.parent.name == 'enemy'){
                contact.disabled = true;
            }
            if(contact.getWorldManifold().normal.x != 0 && other.node.name != 'mario'){
                this.speed *= -1;
            }
            
        }
    }

}
