
const {ccclass, property} = cc._decorator;

@ccclass
export default class leader extends cc.Component {

    @property(cc.Prefab)
    record: cc.Prefab = null;
    
    start(){
        var Ref = firebase.database().ref('leaderboard');
        Ref.once('value').then((snapshot)=>{
            if(snapshot.val()){
                var child = snapshot.val();
                for(var i=0; ; i++){
                    if(!child.score[i] || i>=6) break;
                    var newn = cc.instantiate(this.record);
                    newn.getChildByName('name').getComponent(cc.Label).string = child.usr[i];
                    newn.getChildByName('score').getComponent(cc.Label).string = child.score[i].toString();
                    newn.parent = cc.find('Canvas/Main Camera/leaderboard');

                }
            }
        });
    }
}
