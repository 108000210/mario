
const {ccclass, property} = cc._decorator;

@ccclass
export default class qbox extends cc.Component {

    

    // LIFE-CYCLE CALLBACKS:
    @property(cc.Prefab)
    private coin_out: cc.Prefab = null;

    @property
    public type: number = 0;

    @property(cc.Prefab)
    private green_mush: cc.Prefab = null;
    

    private anim: cc.Animation = null;

    private emp_box: cc.SpriteFrame = null;
    // onLoad () {}
    private emp: boolean = false;

    start () {
        cc.director.getCollisionManager().enabled = true;
        this.anim = this.getComponent(cc.Animation);
        this.emp_box = this.getComponent(cc.Sprite).spriteFrame;
    }

    // update (dt) {}

    onCollisionEnter(other, self) {
        //cc.log('cllis');
        if(other.node.name == 'mario' && !this.emp){
            let pre_player = other.world.preAabb;
            let pre_box = self.world.preAabb;
            cc.log(pre_player.y <= pre_box.y);
            cc.log(Math.abs(pre_player.x-pre_box.x));
            if(pre_player.y <= pre_box.y && Math.abs(pre_player.x-pre_box.x) <= 28){
                this.anim.stop();
                let ac = cc.moveBy(0.1, 0, 10);
                let ac2 = cc.moveBy(0.1, 0, -10);
                let ac3 = cc.callFunc(()=>{this.getComponent(cc.Sprite).spriteFrame = this.emp_box;});
                this.node.runAction(cc.sequence(ac, ac2, ac3));
                if(this.type == 0){
                    this.call_coin();
                }else if(this.type == 1){
                    this.call_green_mush();
                }
                this.emp = true;
            }
        }
    }
    call_green_mush(){
        let gmush = cc.instantiate(this.green_mush);
        gmush.parent = this.node.parent;
        gmush.position = cc.v2(this.node.position.x, this.node.position.y + 20);
        gmush.getComponent('green_mush').show();
        //cc.find("gameM").getComponent('gameM').player_score += 10;
        //cc.log(cc.find("gameM").getComponent('gameM').player_score);
    }
    call_coin(){
        let coin = cc.instantiate(this.coin_out);
        coin.parent = this.node.parent;
        coin.position = this.node.position;
        coin.getComponent('coin_out').show();
        
        cc.find("gameM").getComponent('gameM').player_score += 10;
        //cc.log(cc.find("gameM").getComponent('gameM').player_score);
    }
    reload(){
        this.emp = false;
        this.anim.play('qbox');
    }
}
