
const {ccclass, property} = cc._decorator;

@ccclass
export default class brick extends cc.Component {

    @property(cc.Prefab)
    private coin_out: cc.Prefab = null;

    private anim: cc.Animation = null;

    //private emp_box: cc.SpriteFrame = null;
    // onLoad () {}
    //private emp: boolean = false;

    start () {
        this.anim = this.getComponent(cc.Animation);
        
    }

    // update (dt) {}

    onCollisionEnter(other, self) {
        //cc.log('cllis');
        if(other.node.name == 'mario'){
            let pre_player = other.world.preAabb;
            let pre_box = self.world.preAabb;
            if(pre_player.y <= pre_box.y && Math.abs(pre_player.x-pre_box.x) <= 28){
                this.call_coin();
                this.anim.play('hit_brick');
                this.scheduleOnce(()=>{this.node.active = false;}, 0.3);
            }
        }
    }
    
    call_coin(){
        let coin = cc.instantiate(this.coin_out);
        coin.parent = this.node.parent;
        coin.position = this.node.position;
        coin.getComponent('coin_out').show();
        cc.find("gameM").getComponent('gameM').player_score += 10;
        //cc.log(cc.find("gameM").getComponent('gameM').player_score);
    }

    reload(){
        this.node.active = true;
        this.anim.play('brick');
    }
}
