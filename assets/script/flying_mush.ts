
const {ccclass, property} = cc._decorator;

@ccclass
export default class flying_mush extends cc.Component {

    private anim: cc.Animation = null;

    private dead_sprite: cc.SpriteFrame = null;

    private init_pos: cc.Vec2 = null;

    @property({ type: cc.AudioClip })
    private kick_bgm: cc.AudioClip = null;
    onLoad(){
        cc.director.getCollisionManager().enabled = true;
    }
    start () {
        
        this.init_pos = this.node.position;
        this.dead_sprite = this.getComponent(cc.Sprite).spriteFrame;
        this.anim = this.getComponent(cc.Animation);
        this.comout();
        this.schedule(this.comout, 7);
    }

    comout(){
        this.node.runAction(this.action);
        setTimeout(()=>{this.node.runAction(this.action2);},4000);
    }
    private action = cc.moveBy(2, 0, 300);
    private action2 = cc.moveBy(2, 0, -300);

    onCollisionEnter(other, self) {
        //cc.log('cllis');
        if(other.node.name == 'mario' && !other.node.getComponent('player').isReBorn){
            let pre_player = other.world.aabb;
            let pre_mush = self.world.aabb;
            
            if(pre_player.y > pre_mush.y && Math.abs(pre_player.x-pre_mush.x)<=35){
                this.anim.stop();
                this.getComponent(cc.Sprite).spriteFrame = this.dead_sprite;
                //setTimeout(this.destroy_item, 1500);
                this.node.stopAllActions();
                cc.find("gameM").getComponent('gameM').player_score += 10;
                this.unschedule(this.comout);
                setTimeout(()=>{this.node.active = false;}, 200);
                cc.audioEngine.playEffect(this.kick_bgm,false);
            }else{
                if(other.node.getComponent('player').player_status == 2){
                    other.node.getComponent('player').turn_red_anim();
                }
                cc.find("Canvas/mario").getComponent('player').player_status -= 1;
            }
        }
    }

    reload(){
        //this.unschedule(this.comout);
        if(!this.node.active){
            this.node.position = this.init_pos;
            this.node.active = true;
            this.comout(); 
            this.schedule(this.comout, 7);
            this.anim.play('fly_mush');
        }
    }
}
