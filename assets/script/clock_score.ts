
const {ccclass, property} = cc._decorator;

@ccclass
export default class clock_score extends cc.Component {

    
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    @property(cc.Node)
    private gameM: cc.Node = null;

    update (dt) {
        if(this.node.name == 'clock'){
            this.node.getComponent(cc.Label).string = this.gameM.getComponent('gameM').clock;
        }else if(this.node.name == 'score'){
            this.node.getComponent(cc.Label).string = this.gameM.getComponent('gameM').player_score;
        }else if(this.node.name == 'life'){
            this.node.getComponent(cc.Label).string = this.gameM.getComponent('gameM').player_life;
        }
    }
}
