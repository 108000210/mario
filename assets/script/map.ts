
const {ccclass, property} = cc._decorator;

@ccclass
export default class map extends cc.Component {

    
    //onLoad () {}

    //start () {}

    //update (dt) {}

    onBeginContact(contact, self, other) {
        if(self.node.name == 'high_rock_land' || self.node.name == 'high_grass1' || self.node.name == 'high_grass2' || self.node.name == 'land7'){
            if(contact.getWorldManifold().normal.x != 0 || contact.getWorldManifold().normal.y != 1){
                contact.disabled = true;
            }
        }
    }
}
