
const {ccclass, property} = cc._decorator;

@ccclass
export default class player extends cc.Component {

    @property({ type: cc.AudioClip })
    private power_up: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    private power_down: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    private jump_sound: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    private die: cc.AudioClip = null;

    private right: boolean = false;

    private left: boolean = false;
    
    private jump: boolean = false;

    private onGround: boolean = false;

    private isDead: boolean = false;

    public isReBorn: boolean = false;

    private playerSpeed: number = 500;

    private moveDir: number = 0;

    private anim: cc.Animation = null;

    private idleFrame: cc.SpriteFrame = null;

    private initPos: cc.Vec2;
    
    @property
    public player_status = 1;
    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().gravity = cc.v2 (0, -200); 
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    
    }
    start () {
        this.idleFrame = this.getComponent(cc.Sprite).spriteFrame;
        this.anim = this.getComponent(cc.Animation);
        this.initPos = this.node.position;
        
    }
    
    update (dt) {
        this.playerMovement(dt);
        //this.play_anim(dt);
        this.update_status(dt);
    }
    onKeyDown(event) {
        cc.log("Key Down: " + event.keyCode);
        if(event.keyCode == cc.macro.KEY.right) {
            this.moveDir = 1;
            this.right = true;
            this.left = false;
        } else if(event.keyCode == cc.macro.KEY.left) {
            this.moveDir = -1;
            this.right = false;
            this.left = true;
        } else if(event.keyCode == cc.macro.KEY.space) {
            this.jump = true;
        }
    }
    onKeyUp(event){
        if(event.keyCode == cc.macro.KEY.right)
            this.moveDir = 0;
        else if(event.keyCode == cc.macro.KEY.left)
            this.moveDir = 0;
        else if(event.keyCode == cc.macro.KEY.space)
            this.jump = false;
    }
    onBeginContact(contact, self, other) {
        //if(other.node.name == 'land9') cc.log(contact.getWorldManifold().normal);
        if(contact.getWorldManifold().normal.x == 0 && contact.getWorldManifold().normal.y < 0){
            //cc.log('hey');
            if(other.node.parent.name == 'map'){
                this.onGround = true;
            }else if(other.node.parent.name == 'map2'){
                this.onGround = true;
                //cc.log('yes');
            }
        }else{
            //cc.log('hey2');
            if(other.node.name == 'hill'){
                this.onGround = true;
            }else if(other.node.parent.name  == 'hill'){
                this.onGround = true;
            }
        }
        
    }
    private playerMovement(dt) {

        if(this.moveDir == 1){
            //this.getComponent(cc.RigidBody).applyForceToCenter(new cc.Vec2(1500, 0), true);
            if(this.getComponent(cc.RigidBody).linearVelocity.x < 550)
                this.getComponent(cc.RigidBody).applyForceToCenter(new cc.Vec2(5000, 0), true);
                
            if(this.node.scaleX > 0) this.node.scaleX *= -1;
            //this.node.scaleX = -1;
        }
        else if(this.moveDir == -1){
            if(this.getComponent(cc.RigidBody).linearVelocity.x > -550)
                this.getComponent(cc.RigidBody).applyForceToCenter(new cc.Vec2(-5000, 0), true);
            if(this.node.scaleX < 0) this.node.scaleX *= -1;
        }
        
        if(this.jump && this.onGround)
            this.to_jump();
    }

    private play_anim_1(){
        if(this.isReBorn){
            if(!this.anim.getAnimationState("turn_color").isPlaying)
                this.anim.play('turn_color');
        }else if(this.jump){
            if(!this.anim.getAnimationState("jump").isPlaying)
                this.anim.play("jump");
        }else if(this.moveDir != 0){
            if(!this.anim.getAnimationState("mario_run").isPlaying)
                this.anim.play("mario_run");
        }else{
            this.anim.stop();
            this.getComponent(cc.Sprite).spriteFrame = this.idleFrame;
        }

    }
    private play_anim_2(){
        if(this.isReBorn){
            if(!this.anim.getAnimationState("turn_color").isPlaying)
                this.anim.play('turn_color');
        }else if(this.jump){
            if(!this.anim.getAnimationState("green_jump").isPlaying)
                this.anim.play("green_jump");
        }else if(this.moveDir != 0){
            if(!this.anim.getAnimationState("green_run").isPlaying)
                this.anim.play("green_run");
        }else{
            if(!this.anim.getAnimationState("green").isPlaying)
                this.anim.play("green");
        }

    }

    private to_jump(){
        //cc.log('jump!');
        this.onGround = false;
        this.getComponent(cc.RigidBody).applyForceToCenter(new cc.Vec2(0, 110000), true);
        cc.audioEngine.playEffect(this.jump_sound, false);
    }
    
    private update_status(dt){
        //if(this.node.p)
        if(this.node.getBoundingBoxToWorld().y < 0){
            this.player_status = 0;
        }
        if(this.player_status == 0){
            if(cc.find("gameM").getComponent('gameM').player_life > 1){
                cc.log('life-1');
                cc.find("gameM").getComponent('gameM').player_life -= 1;
                this.player_status = 1;
                this.reload_everything();
                this.reborn(cc.v2(0,0));
                //cc.find("gameM").getComponent('gameM').player_life -= 1;
            }else if(cc.find("gameM").getComponent('gameM').player_life <= 1){
                cc.director.loadScene('lose');
            }
        }else if(this.player_status == 1){
            this.play_anim_1();
        }else if(this.player_status == 2){
            this.play_anim_2();
        }

    }

    reborn(rebornPos: cc.Vec2){
        this.node.position = rebornPos;
        this.getComponent(cc.RigidBody).linearVelocity = cc.v2();
    }
    reload_everything(){
        cc.find("gameM").getComponent('gameM').reload();
        cc.find("Canvas/enemy").children.forEach(n=>{
            if(n.name == 'flying_mush'){
                n.getComponent('flying_mush').reload();
            }else if(n.name == 'turtle'){
                n.getComponent('turtle').reload();
            }
        });
        cc.find("Canvas/box").children.forEach(n=>{
            if(n.name == 'Qbox'){
                n.getComponent('qbox').reload();
            }else if(n.name == 'brick'){
                n.getComponent('brick').reload();
            }
        });
    }
    
    turn_green_anim(){
        cc.audioEngine.playEffect(this.power_up, false);
        this.isReBorn = true;
        this.scheduleOnce(()=>{
            this.isReBorn = false;
        },0.5);
    }

    turn_red_anim(){
        cc.audioEngine.playEffect(this.power_down, false);
        this.isReBorn = true;
        this.scheduleOnce(()=>{
            this.isReBorn = false;
        },0.5);
    }
}
