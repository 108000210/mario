import { resolve } from "dns";

const {ccclass, property} = cc._decorator;

@ccclass
export default class gameM extends cc.Component {

    @property(cc.Prefab)
    private text: cc.Prefab = null;

    @property(cc.Prefab)
    private but: cc.Prefab = null;

    @property({ type: cc.AudioClip })
    private game_bgm: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    private menu_bgm: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    private lose_bgm: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    private die: cc.AudioClip = null;
    

    public player_life: number = 5;
    // LIFE-CYCLE CALLBACKS:
    public player_score: number = 0;

    public clock: number = null;
    //onLoad () {}
    private array_score: number[];
    
    private array_usr: string[];

    private flag: boolean = false;

    private s: string = null;

    start () {
        this.s = cc.director.getScene().name;
        var cur_scene = cc.director.getScene().name;
        if(cur_scene == 'game'){
            cc.audioEngine.playMusic(this.game_bgm, true);
            this.clock = 60;
            this.schedule(this.set_clock, 1);
            this.load_usr_info();
        }else if(cur_scene == 'menu'){
            cc.audioEngine.playMusic(this.menu_bgm, true);
        }else if(cur_scene == 'game2'){
            cc.audioEngine.playMusic(this.game_bgm, true);
            this.clock = 120;
            this.schedule(this.set_clock, 1);
            this.load_usr_info();
        }else if(cur_scene == 'lose'){
            cc.log('start');
            this.lose_store_usr_info();
            cc.audioEngine.playMusic(this.lose_bgm, false);
        }
        //this.load_usr_info();
        //cc.log(cc.director.getScene().name);
    }
    set_clock(){
        this.clock -= 1;
    }
    update(dt){
        if(this.s == 'game' || this.s == 'game2'){
            if(this.clock <= 0 && !this.flag){
                cc.log('resetclock');
                this.flag = true;
                cc.find('Canvas/mario').getComponent('player').player_status = 0;
            }
        }
    }

    lose_store_usr_info(){
        var user = firebase.auth().currentUser;
        if(user){
            var usr_email = user.email.replace(/\./g, ',');
            var Ref = firebase.database().ref('player/'+usr_email);
            Ref.once('value').then((snapshot)=>{
                if(snapshot.val()){
                    //cc.log(snapshot.val());
                    var Data = snapshot.val();
                    var data = {
                        player: user.email,
                        top_score: Data.top_score,
                        player_life: 5,
                    }
                    Ref.update(data);
                    cc.log('stored');
                }else{
                    var data2 = {
                        player: user.email,
                        top_score: 0,
                        player_life: 5,
                    }
                    Ref.update(data2);
                }
            }).catch((error)=>{
                var err_mes = error.message;
                cc.log(err_mes);
            }); 
        }
    }
    reload(){
        var user = firebase.auth().currentUser;
        if(user){
            var usr_email = user.email.replace(/\./g, ',');
            var Ref = firebase.database().ref('player/'+usr_email);
            Ref.once('value').then((snapshot)=>{
                if(snapshot.val()){
                    //cc.log(snapshot.val());
                    var Data = snapshot.val();
                    var data = {
                        player: user.email,
                        top_score: Data.top_score,
                        player_life: this.player_life,
                    }
                    Ref.update(data);
                }else{
                    var data2 = {
                        player: user.email,
                        top_score: 0,
                        player_life: this.player_life,
                    }
                    Ref.update(data2);
                }
            }).then(()=>{
                this.reload_scene();
            }); 
        }else{
            this.reload_scene();
        }
    }
    reload_scene(){
        this.player_score = 0;
        var cur_scene = cc.director.getScene().name;
        cc.audioEngine.playMusic(this.die, false);
        this.scheduleOnce(()=>{cc.audioEngine.playMusic(this.game_bgm, true);}, 2);
        this.unschedule(this.set_clock);
        if(cur_scene == 'game'){
            this.clock = 60;
            this.schedule(this.set_clock, 1);
            this.flag = false;
        }else if(cur_scene == 'game2'){
            this.clock = 120;
            this.schedule(this.set_clock, 1);
            this.flag = false;
        }
    }
    load_usr_info(){
        //var user = firebase.auth().currentUser;
        firebase.auth().onAuthStateChanged(user => {
            if(user){
                var usr_email = user.email.replace(/\./g, ',');
                var Ref = firebase.database().ref('player/'+usr_email);
                Ref.once('value').then((snapshot)=>{
                    if(snapshot.val()){
                        var Data = snapshot.val();
                        this.player_life = Data.player_life;
                        //cc.log(Data);
                    }
                }); 
            }
        });
    }
    store_usr_info(){
        var user = firebase.auth().currentUser;
        if(user){
            var usr_email = user.email.replace(/\./g, ',');
            var Ref = firebase.database().ref('player/'+usr_email);
            var score = this.player_score;
            Ref.once('value').then((snapshot)=>{
                if(snapshot.val()){
                    //cc.log(snapshot.val());
                    var Data = snapshot.val();
                    if(Data.top_score > score) score = Data.top_score;
                    var data = {
                        player: user.email,
                        top_score: score,
                        player_life: this.player_life,
                    }
                    Ref.update(data);
                }else{
                    var data = {
                        player: user.email,
                        top_score: score,
                        player_life: this.player_life,
                    }
                    Ref.update(data);
                }
            }); 
        }
    }
    load_win_scene(){
        this.unschedule(this.set_clock);
        cc.find('Canvas/win').active = true;
        cc.find('Canvas/mario').active = false;
        var vic_label = cc.instantiate(this.text);
        vic_label.getComponent(cc.Label).string = "VICTORY!";
        vic_label.parent = cc.find('Canvas/Main Camera');

        var button_menu = cc.instantiate(this.but);
        var button_leader_board = cc.instantiate(this.but);
        button_menu.parent = cc.find('Canvas/Main Camera');
        button_leader_board.parent = cc.find('Canvas/Main Camera');
        button_menu.getChildByName('Background').getChildByName('Label').getComponent(cc.Label).string = "MENU";
        button_leader_board.getChildByName('Background').getChildByName('Label').getComponent(cc.Label).string = "LEADERBOARD";
        var hit_menu = new cc.Component.EventHandler();
        hit_menu.target = this.node;
        hit_menu.component = "gameM";
        hit_menu.handler = "goto_menu";
        button_menu.getComponent(cc.Button).clickEvents.push(hit_menu);
        var hit_l = new cc.Component.EventHandler();
        hit_l.target = this.node;
        hit_l.component = "gameM";
        hit_l.handler = "goto_l";
        button_leader_board.getComponent(cc.Button).clickEvents.push(hit_l);
        button_menu.position = cc.v2(0,-100);
        button_leader_board.position = cc.v2(0, -200);
    }
    goto_menu(){
        cc.director.loadScene('menu');
    }
    goto_l(){
        var user = firebase.auth().currentUser;
        if(user){
            var usr_email = user.email.replace(/\./g, ',');
            var Ref = firebase.database().ref('player');
            Ref.once('value').then((snapshot)=>{
                if(snapshot.val()){
                    this.array_usr = [];
                    this.array_score = [];
                    snapshot.forEach((childshot)=>{
                        var Data = childshot.val();
                        this.array_score.push(Data.top_score);
                        this.array_usr.push(Data.player);
                    });
                    this.sort();
                    var ref = firebase.database().ref('leaderboard');
                    var data = {
                        score: this.array_score,
                        usr: this.array_usr
                    }
                    ref.update(data).then(()=>{
                        cc.director.loadScene('leader');
                    });
                }
            });
        }else{
            alert('You have to log in to check leaderboard.');
        }
    }
    sort(){
        var n = this.array_score.length;
        for(var i=n-1; i>=0; i--){
            for(var j=0; j<i; j++){
                if(this.array_score[j] < this.array_score[j+1]){
                    var tmp = this.array_score[j];
                    this.array_score[j] = this.array_score[j+1];
                    this.array_score[j+1] = tmp;
                    var tmp2 = this.array_usr[j];
                    this.array_usr[j] = this.array_usr[j+1];
                    this.array_usr[j+1] = tmp2;
                }
            }
        }
    }
    logout(){
        firebase.auth().signOut().then(()=>{
            cc.log('logout success!');
            alert('log out success!');
        });
    }
}
