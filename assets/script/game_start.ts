
const {ccclass, property} = cc._decorator;

@ccclass
export default class game_start extends cc.Component {

    
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        var cur_scene = cc.director.getScene().name;
        cc.audioEngine.stopAll();
        if(cur_scene == 'world1_start'){
            this.scheduleOnce(()=>{cc.director.loadScene('game');},3);

        }else if(cur_scene == 'world2_start'){
            this.scheduleOnce(()=>{cc.director.loadScene('game2');},3);
        }
    }

    // update (dt) {}
}
