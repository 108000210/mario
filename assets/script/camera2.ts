
const {ccclass, property} = cc._decorator;

@ccclass
export default class camera2 extends cc.Component {

    @property(cc.Node)
    player: cc.Node = null;

    private targetPos: cc.Vec2;

    private curPos: cc.Vec2;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    
    
    update (dt) {
        this.targetPos = this.player.position;
        this.curPos = this.node.position;
        this.targetPos.y = cc.misc.clampf(this.targetPos.y, 0, 640);

        this.curPos.lerp(this.targetPos, 0.1, this.curPos);

        this.curPos.y = cc.misc.clampf(this.targetPos.y, 0, 400);
        this.curPos.x = cc.misc.clampf(this.targetPos.x, 0, 7200);
        this.node.setPosition(this.curPos);
    }
    
}
