
const {ccclass, property} = cc._decorator;

@ccclass
export default class flower extends cc.Component {

    

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.comout();
        this.schedule(this.comout, 5);
    }

    comout(){
        this.node.runAction(this.action);
        setTimeout(()=>{this.node.runAction(this.action2);},3000);
    }
    private action = cc.moveBy(1, 0, 75);
    private action2 = cc.moveBy(1, 0, -75);

    onBeginContact(contact, self, other){
        if(other.node.name == 'mario' && !other.node.getComponent('player').isReBorn){
            //cc.log(cc.find("Canvas/mario"));
            if(other.node.getComponent('player').player_status == 2){
                other.node.getComponent('player').turn_red_anim();
            }
            cc.find("Canvas/mario").getComponent('player').player_status -= 1;
        }
    }

    // update (dt) {}
}
